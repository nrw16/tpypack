from setuptools import setup

setup(name='newpack',
      version='0.0.1',
      description='does some cool stuff',
      url='www.python.org/',
      author='author name',
      author_email='author@email',
      license='GPL3',
      packages=['newpack'],
      package_dir={'newpack': './newpack'},
      package_data={'newpack': ['files/data/*','files/docs/*','README.rst']},
      install_requires=['markdown'],
      include_package_data=True,
      zip_safe=False)
